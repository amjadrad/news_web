<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNewsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_news_media', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('user_news_id');
            $table->string('path', 250);
            $table->enum('type', ['picture' , 'video'])->default('picture');

            $table->foreign('user_news_id')->references('id')->on('user_news');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_news_media');
    }
}
