<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_comments', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('reply_id')->nullable();
            $table->string('comment', 250);
            $table->boolean('active')->default(0);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('news_id')->references('id')->on('news');
            $table->foreign('reply_id')->references('id')->on('news_comments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_comments');
    }
}
