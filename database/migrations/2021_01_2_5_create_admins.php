<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('full_name', 128)->nullable();
            $table->string('phone', 11)->unique();
            $table->string('verification_code', 4)->nullable();
            $table->string('api_token', 256)->nullable();
            $table->boolean('active')->default(0);
            $table->enum('type', ['owner', 'general'])->default('general');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
