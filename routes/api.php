<?php

use App\Http\Controllers\API\AdminsAccessController;
use App\Http\Controllers\API\AdminsController;
use App\Http\Controllers\API\AdminSettingsController;
use App\Http\Controllers\API\AdminUserNewsController;
use App\Http\Controllers\API\AppCommentsController;
use App\Http\Controllers\API\CategoriesController;
use App\Http\Controllers\API\CitiesController;
use App\Http\Controllers\API\ContactsController;
use App\Http\Controllers\API\NewsCommentsController;
use App\Http\Controllers\API\NewsController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\UserNewsController;
use App\Http\Controllers\API\UserNewsFavoriteController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

//Route::post('upload_file', function (Request $request) {
//
//    if ($request->has('upload_file')) {
//        $fileName = 'news_' . $request->upload_file->getClientOriginalName();
//        Storage::disk('public')
//            ->put($fileName, file_get_contents($request->upload_file->getRealPath()));
//        return response(['status' => 200]);
//    } else if ($request->has('base64_file')) {
////        $arr = explode(':', $request->base64_file);
//        $image = base64_decode($request->base64_file);
//        $fileName = 'news_' . $request->file_name;
//        Storage::disk('public')
//            ->put($fileName, $image);
//        return response(['status' => 200]);
//    } else {
//        return response(['status' => 404], 404);
//    }
//
//});


Route::post('upload_file', function (Request $request) {

    if ($request->has('upload_file')) {
        $fileName = $request->upload_file->getClientOriginalName();
        Storage::disk('public')
            ->put($fileName, file_get_contents($request->upload_file->getRealPath()));
        return response(['status' => 200]);
    } else if ($request->has('base64_file')) {
//        $arr = explode(':', $request->base64_file);
        $image = base64_decode($request->base64_file);
        $fileName = 'news_' . $request->file_name;
        Storage::disk('public')
            ->put($fileName, $image);
        return response(['status' => 200]);
    } else {
        return response(['status' => 404], 404);
    }

});

//Route::get('/force_update', [AppController::class, 'apiCheckForceUpdate']);
Route::post('signin', [UserController::class, 'signin']);
Route::post('verify', [UserController::class, 'signinVerification']);
Route::get('user/edit', [UserController::class, 'edit']);
Route::post('user/update', [UserController::class, 'update']);
Route::get('provinces', [CitiesController::class, 'provinces']);
Route::get('cities', [CitiesController::class, 'cities']);
Route::get('categories/index', [CategoriesController::class, 'index']);

Route::get('user/count', [UserController::class, 'count']);

//AppComments
Route::post('app/comment/store', [AppCommentsController::class, 'store']);
Route::post('admin/app/comment/delete', [AppCommentsController::class, 'adminDelete']);
Route::get('admin/app/comment/index', [AppCommentsController::class, 'adminIndex']);
Route::get('admin/app/comment/average_rating', [AppCommentsController::class, 'adminAverageRating']);


Route::get('/user_news_favorites/index', [UserNewsFavoriteController::class, 'index']);
Route::post('/user_news_favorites/store', [UserNewsFavoriteController::class, 'store']);
Route::post('/user_news_favorites/delete', [UserNewsFavoriteController::class, 'delete']);

Route::get('news/search', [NewsController::class, 'search']);
Route::get('news/hot', [NewsController::class, 'getHotNews']);
Route::get('news/most_visit', [NewsController::class, 'getMostVisitNews']);
Route::get('news/posters/main', [NewsController::class, 'getPosterMainNews']);
Route::get('news/show', [NewsController::class, 'show']);
Route::post('news/like', [NewsController::class, 'like']);
Route::post('news/visit', [NewsController::class, 'visit']);

//Contact us
Route::get('admin/contacts/index', [ContactsController::class, 'adminIndex']);
Route::post('contacts/store', [ContactsController::class, 'store']);
Route::post('admin/contacts/seen', [ContactsController::class, 'adminSeen']);
Route::post('admin/contacts/delete', [ContactsController::class, 'adminDelete']);


Route::get('news/comments/index', [NewsCommentsController::class, 'index']);
Route::post('news/comments/store', [NewsCommentsController::class, 'store']);
Route::post('news/comments/reply', [NewsCommentsController::class, 'reply']);
Route::post('admin/news/comments/delete', [NewsCommentsController::class, 'delete']);
Route::get('admin/news/comments/index', [NewsCommentsController::class, 'adminIndex']);
Route::post('admin/news/comments/active', [NewsCommentsController::class, 'adminActive']);
Route::post('admin/news/comments/seen', [NewsCommentsController::class, 'adminSeen']);

//Admin
Route::get('admin/admins/index', [AdminsController::class, 'adminIndex']);
Route::post('admin/admins/delete', [AdminsController::class, 'adminDelete']);
Route::post('admin/admins/update', [AdminsController::class, 'adminUpdate']);
Route::post('admin/admins/store', [AdminsController::class, 'adminStore']);
Route::get('admin/admins/show', [AdminsController::class, 'adminShow']);
//Admin access
Route::get('admin/admins/access/edit', [AdminsAccessController::class, 'edit']);
Route::post('admin/admins/access/update', [AdminsAccessController::class, 'update']);
//Admin Settings
Route::get('admin/settings/edit', [AdminSettingsController::class, 'edit']);
Route::post('admin/settings/update', [AdminSettingsController::class, 'update']);

//Admin news
Route::get('admin/news/search', [NewsController::class, 'adminSearch']);
Route::get('admin/news/show', [NewsController::class, 'adminShow']);
Route::get('admin/news/edit', [NewsController::class, 'adminEdit']);
Route::post('admin/news/delete', [NewsController::class, 'adminDelete']);
Route::post('admin/news/update', [NewsController::class, 'adminUpdate']);
Route::post('admin/news/store', [NewsController::class, 'adminStore']);
Route::post('admin/news/media/delete', [NewsController::class, 'adminMediaDelete']);

Route::get('admin/categories/index', [CategoriesController::class, 'adminIndex']);
Route::get('admin/categories/show', [CategoriesController::class, 'adminShow']);
Route::post('admin/categories/delete', [CategoriesController::class, 'adminDelete']);
Route::post('admin/categories/update', [CategoriesController::class, 'adminUpdate']);
Route::post('admin/categories/store', [CategoriesController::class, 'adminStore']);

//User news
Route::get('user/news/index', [UserNewsController::class, 'index']);
Route::get('user/news/show', [UserNewsController::class, 'show']);
Route::get('user/news/edit', [UserNewsController::class, 'edit']);
Route::post('user/news/delete', [UserNewsController::class, 'delete']);
Route::post('user/news/update', [UserNewsController::class, 'update']);
Route::post('user/news/store', [UserNewsController::class, 'store']);
Route::post('user/news/media/delete', [UserNewsController::class, 'mediaDelete']);
//Admin User news
Route::get('admin/user/news/index', [AdminUserNewsController::class, 'index']);
Route::get('admin/user/news/show', [AdminUserNewsController::class, 'show']);
Route::get('admin/user/news/edit', [AdminUserNewsController::class, 'edit']);
Route::post('admin/user/news/delete', [AdminUserNewsController::class, 'delete']);
Route::post('admin/user/news/update', [AdminUserNewsController::class, 'update']);
Route::post('admin/user/news/reject', [AdminUserNewsController::class, 'reject']);
Route::post('admin/user/news/store', [AdminUserNewsController::class, 'store']);
Route::post('admin/user/news/media/delete', [AdminUserNewsController::class, 'mediaDelete']);
