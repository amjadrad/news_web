<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    public function adminIndex(Request $request)
    {
        $data = Admin::all();
        return response(['status' => 200, 'admins' => $data], 200);
    }

    public function adminShow(Request $request)
    {
        $admin = Admin::find($request->admin_id);
        if ($admin) {
            return response(['status' => 200, 'admin' => $admin], 200);
        } else {
            return response(['status' => 404], 404);
        }
    }

    public function adminStore(Request $request)
    {
        $admin = new Admin();
        $admin->full_name = $request->full_name;
        $admin->phone = $request->phone;
        $admin->save();
        return response(['status' => 200, 'admin' => $admin], 200);
    }

    public function adminUpdate(Request $request)
    {
        $admin = Admin::find($request->admin_id);
        if ($admin) {
            $admin->full_name = $request->full_name;
            $admin->save();
            return response(['status' => 200, 'admin' => $admin], 200);
        } else {
            return response(['status' => 404], 404);
        }
    }

    public function adminDelete(Request $request)
    {
        $admin = Admin::find($request->admin_id);
        if ($admin->type == 'owner') {
            return response(['status' => 401,], 401);
        } else {
            if (count($admin->news) == 0) {
                $admin->delete();
                return response(['status' => 200, 'admin_id' => $request->admin_id], 200);
            } else {
                return response(['status' => 403,], 403);
            }
        }
    }
}
