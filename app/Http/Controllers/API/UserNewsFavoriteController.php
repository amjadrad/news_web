<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserNewsFavorite;
use Illuminate\Http\Request;

class UserNewsFavoriteController extends Controller
{
    public function index(Request $request)
    {
        $data = UserNewsFavorite::where('user_id', $request->user_id)->with(['news' => function ($query) {
            $query->select('id', 'title', 'created_at' , 'category_id')->with('category');
        }])->orderBy('id','DESC')->paginate(20);
        return response(['status' => 200, 'user_news_favorites' => $data]);
    }

    public function store(Request $request)
    {
        $deleted = false;
        $item = UserNewsFavorite::where('user_id', $request->user_id)->where('news_id', $request->news_id)->first();
        if ($item) {
            $deleted = true;
            $item->delete();
        } else {
            $item = new UserNewsFavorite();
            $item->user_id = $request->user_id;
            $item->news_id = $request->news_id;
            $item->save();
        }
        return response(['status' => 201, 'news_id' => $request->news_id, 'favorite' => $item, 'deleted' => $deleted], 201);
    }

    public function delete(Request $request)
    {
        $item = UserNewsFavorite::find($request->user_news_favorite_id);
        $item->delete();
        return response(['status' => 200, 'user_news_favorite_id' => $request->user_news_favorite_id]);
    }
}
