<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserNews;
use App\Models\UserNewsMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserNewsController extends Controller
{

    public function index(Request $request)
    {
        $userId = $request->user_id;
        $paginatedData = UserNews::orderBy('created_at', 'DESC')
            ->where('user_id', $userId)
            ->select('id', 'title', 'category_id', 'status', 'created_at')
            ->with('category');
        $paginatedData = $paginatedData->paginate(15);

        return response(['status' => 200, 'data' => $paginatedData, 'next_page_url' => $paginatedData->nextPageUrl()]);
    }

    public function show(Request $request)
    {
        $news = UserNews::where('id', $request->user_news_id)->with('category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function delete(Request $request)
    {
        $news = UserNews::with('medias')->find($request->user_news_id);
        foreach ($news->medias as $media) {
            Storage::disk('public')->delete($media->path);
        }
        $news->medias()->delete();
        $news->delete();
        return response(['status' => 200, 'user_news_id' => $request->user_news_id], 200);
    }

    public function store(Request $request)
    {
        $news = new UserNews();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->user_id = $request->user_id;
        $news->category_id = $request->category_id;
        $news->save();

        if ($request->media) {
            foreach ($request->media as $media) {
                $newMedia = new UserNewsMedia();
                $newMedia->user_news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }

        return response(['status' => 200, 'user_news_id' => $news->id], 200);
    }

    public function edit(Request $request)
    {
        $news = UserNews::where('id', $request->user_news_id)->with('medias', 'category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function update(Request $request)
    {
        $news = UserNews::find($request->user_news_id);
        $news->title = $request->title;
        $news->description = $request->description;
        $news->category_id = $request->category_id;
        $news->save();
        if ($request->media) {
            foreach ($request->media as $media) {
                $newMedia = new UserNewsMedia();
                $newMedia->user_news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }

        return response(['status' => 200, 'news_id' => $news->id], 200);
    }

    public function mediaDelete(Request $request)
    {
        $newsMedia = UserNewsMedia::find($request->media_id);
        Storage::disk('public')->delete($newsMedia->path);
        $newsMedia->delete();
        return response(['status' => 200, 'media_id' => $request->media_id], 200);
    }

}
