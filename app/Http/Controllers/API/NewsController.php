<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\News;
use App\Models\NewsLike;
use App\Models\NewsMedia;
use App\Models\NewsPoster;
use App\Models\NewsVisit;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function show(Request $request)
    {
        $userId = $request->user_id;
        $news = News::where('id', $request->news_id)
            ->where('active', 1)
            ->withCount(['likes', 'dislikes', 'visits', 'comments' => function ($query) {
                $query->where('active', 1);
            }])
            ->with(['medias', 'favorite' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }, 'category', 'visits'])
            ->first();

        $visit = NewsVisit::where('user_id', $request->user_id)->where('news_id', $request->news_id)->first();
        if (!$visit) {
            $visit = new NewsVisit();
            $visit->user_id = $request->user_id;
            $visit->news_id = $request->news_id;
            $visit->save();
        }
        return response(['status' => 200, 'news' => $news]);
    }

    public function search(Request $request)
    {
        $categoryId = $request->category_id;
        $search = ($request->search == null) ? "" : $request->search;
        $userId = $request->user_id;
        $paginatedData = News::where('active', 1)->orderBy('created_at', 'DESC');
        $paginatedData->withCount(['likes', 'dislikes', 'visits', 'comments' => function ($query) {
            $query->where('active', 1);
        }]);
        if ($request->with_media == "true") {
            $paginatedData->with(['medias', 'favorite' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }, 'category']);
        } else {
            $paginatedData->with(['category', 'favorite' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }]);
        }
        $paginatedData->where('title', 'like', '%' . $search . '%');
        if ($categoryId != null) {
            $paginatedData = $paginatedData->where('category_id', $categoryId);
        }
        $paginatedData = $paginatedData->paginate(15);
        $data = $paginatedData->each(function (&$news) use ($userId) {
            $news['user_like'] = $news->getUserLike($userId);
            $news['user_visit'] = $news->getUserVisit($userId);
        });
        return response(['status' => 200, 'data' => ['data' => $data, 'next_page_url' => $paginatedData->nextPageUrl()]], 200);
    }

    public function getHotNews(Request $request)
    {
        $arrRelations = ['category'];
        if ($request->with_media == "true") {
            array_push($arrRelations, 'medias');
        }
        $data = News::where('active', 1)
            ->where('type', 'hot')
            ->with($arrRelations)
            ->orderBy('created_at', 'DESC')
            ->take(15)->get();
        return response(['status' => 200, 'data' => $data], 200);
    }

    public function getMostVisitNews(Request $request)
    {
        $arrRelations = ['category'];
        if ($request->with_media == "true") {
            array_push($arrRelations, 'medias');
        }
        $data = News::where('active', 1)
            ->with($arrRelations)
            ->withCount('visits')
            ->orderBy('visits_count', 'DESC')
            ->take(15)->get();
        return response(['status' => 200, 'data' => $data], 200);
    }

    public function getPosterMainNews(Request $request)
    {
        $data = NewsPoster::where('poster_type', 'main')
            ->with(['news' => function ($query) {
                $query->with('category')->withCount('visits');
            }])->take(15)->get();
        return response(['status' => 200, 'data' => $data], 200);
    }

    public function adminSearch(Request $request)
    {
        $categoryId = $request->category_id;
        $search = ($request->search == null) ? "" : $request->search;
        $adminId = $request->admin_id;
        $paginatedData = News::orderBy('created_at', 'DESC')
            ->where('title', 'like', '%' . $search . '%')
            ->where('admin_id', $adminId)
            ->select('id', 'title', 'category_id', 'created_at')
            ->with('category')
            ->withCount(['comments as comments_count_unseen' => function ($query) {
                $query->where('seen', 0);
            }]);

        if ($categoryId != null) {
            $paginatedData = $paginatedData->where('category_id', $categoryId);
        }
        $paginatedData = $paginatedData->paginate(15);

        return response(['status' => 200, 'data' => $paginatedData, 'next_page_url' => $paginatedData->nextPageUrl()], 200);
    }

    public function adminDelete(Request $request)
    {
        $news = News::with('medias')->find($request->news_id);
        $news->comments()->delete();
        $news->likes()->delete();
        foreach ($news->medias as $media) {
            Storage::disk('public')->delete($media->path);
        }
        $news->medias()->delete();
        $news->visits()->delete();
        $news->delete();
        return response(['status' => 200, 'news_id' => $request->news_id], 200);
    }

    public function adminStore(Request $request)
    {
        $news = new News();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->link = $request->link;
        $news->admin_id = $request->admin_id;
        $news->category_id = $request->category_id;
        $news->active = $request->active;
        $news->type = $request->hot == 0 ? 'normal' : 'hot';
        $news->save();

        if ($request->media) {
            foreach ($request->media as $media) {

                $newMedia = new NewsMedia();
                $newMedia->news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }

        return response(['status' => 200, 'news_id' => $news->id], 200);
    }

    public function adminShow(Request $request)
    {
        $news = News::where('id', $request->news_id)->with('category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function adminEdit(Request $request)
    {
        $news = News::where('id', $request->news_id)->with('medias', 'category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function adminUpdate(Request $request)
    {
        $news = News::find($request->news_id);
        $news->title = $request->title;
        $news->description = $request->description;
        $news->link = $request->link;
        $news->category_id = $request->category_id;
        $news->active = $request->active;
        $news->type = $request->hot == 0 ? 'normal' : 'hot';
        $news->save();
        if ($request->media) {
            foreach ($request->media as $media) {
                $newMedia = new NewsMedia();
                $newMedia->news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }

        return response(['status' => 200, 'news_id' => $news->id], 200);
    }

    public function adminMediaDelete(Request $request)
    {
        $newsMedia = NewsMedia::find($request->media_id);
        Storage::disk('public')->delete($newsMedia->path);
        $newsMedia->delete();
        return response(['status' => 200, 'media_id' => $request->media_id], 200);
    }

    public function like(Request $request)
    {
        $newsLike = NewsLike::where('user_id', $request->user_id)->where('news_id', $request->news_id)->first();
        if ($newsLike) {
            if ($newsLike->like == $request->like) {
                $newsLike->delete();
            } else {
                $newsLike->like = $request->like;
                $newsLike->save();
            }
        } else {
            $newsLike = new NewsLike();
            $newsLike->user_id = $request->user_id;
            $newsLike->news_id = $request->news_id;
            $newsLike->like = $request->like;
            $newsLike->save();
//            return response(['status' => 201, 'news_id' => $request->news_id, 'like' => $request->like], 201);
        }
        $news = News::where('id', $request->news_id)
            ->where('active', 1)
            ->withCount(['likes', 'dislikes'])
            ->first();
        $isUserLiked = $news->getUserLike($request->user_id);
        return response(['status' => 201, 'news_id' => $request->news_id, 'likes_count' => $news->likes_count, 'dislikes_count' => $news->dislikes_count, 'user_like' => $isUserLiked], 201);
    }

    public function visit(Request $request)
    {
        $visit = NewsVisit::where('user_id', $request->user_id)->where('news_id', $request->news_id)->first();
        if (!$visit) {
            $visit = new NewsVisit();
            $visit->user_id = $request->user_id;
            $visit->news_id = $request->news_id;
            $visit->save();
            return response(['status' => 201, 'news_id' => $request->news_id], 201);
        }
    }
}
