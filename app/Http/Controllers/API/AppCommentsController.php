<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AppComment;
use Illuminate\Http\Request;

class AppCommentsController extends Controller
{

    public function store(Request $request)
    {
        $data = new AppComment();
        $data->user_id = $request->user_id;
        $data->comment = $request->comment;
        $data->rate = $request->rate;
        $data->save();
        return response(['status' => 200]);
    }

    public function adminIndex(Request $request)
    {
        $data = AppComment::orderBy('created_at' , 'DESC')->with(['user' => function ($query) {
            $query->select('id', 'full_name');
        }])
            ->paginate(15);
        return response(['status' => 200, 'data' => $data, 'next_page_url' => $data->nextPageUrl()]);
    }

    public function adminAverageRating(Request $request)
    {
        $averageRating = AppComment::average('rate');
        return response(['status' => 200, 'average_rating' => $averageRating]);
    }

    public function adminDelete(Request $request)
    {
        $item = AppComment::find($request->app_comment_id);
        $item->delete();
        return response(['status' => 200, 'app_comment_id' => $request->app_comment_id]);
    }


}
