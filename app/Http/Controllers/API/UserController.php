<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Admin;
use App\Models\AdminSetting;
use App\SmsVerificationCodeHelper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\SmsVerificationUltraFastHelper;

class UserController extends Controller
{
    public function signin(Request $request)
    {
        $phone = $request->phone;
        $isAdmin = false;
        $admin = Admin::where('phone', $phone)->first();
        $user = User::where('phone', $phone)->first();
//        dd($user);
        if ($admin) {
            $isAdmin = true;
        }
        $verificationCode = rand(1111, 9999);
        $apiToken = Str::random(128);


        if ($user) {
            $user->verification_code = $verificationCode;
            $user->api_token = $apiToken;
            $user->save();
        } else {
            $user = new User();
            $user->phone = $request->phone;
            $user->api_token = $apiToken;
            $user->verification_code = $verificationCode;
            $user->save();
        }
        if ($admin) {
            $admin->verification_code = $verificationCode;
            $admin->api_token = $apiToken;
            $admin->save();
        }
        $res = SmsVerificationUltraFastHelper::sms($user->phone, $verificationCode);
        return response(['status' => 200, 'is_admin' => $isAdmin, 'res' => $res]);
    }

    public function signinVerification(Request $request)
    {
        $admin = Admin::where('phone', $request->phone)->first();
        $user = User::where('phone', $request->phone)->with(['city' => function ($query) {
            $query->with(['province' => function ($query) {
                $query->with('centerCity');
            }]);
        }])->first();
        if ($user) {
            if ($user->verification_code == $request->verification_code) {
                $user->active = 1;
                $user->save();
                return response(['status' => 200, 'user' => $user, 'admin' => $admin]);
            } else {
                return response(['status' => 401], 401);
            }
        } else {
            return response(['status' => 404], 404);
        }
    }

    public function update(Request $request)
    {
        $user = User::find($request->user_id);
        $user->full_name = $request->full_name;
        $user->city_id = $request->city_id;
        $user->save();
        $user = User::where('id', $request->user_id)->with(['city' => function ($query) {
            $query->with(['province' => function ($query) {
                $query->with('centerCity');
            }]);
        }])->first();
        return response(['status' => 200, 'user' => $user]);
    }

    public function edit(Request $request)
    {
        $user = User::where('id', $request->user_id)->with(['city' => function ($query) {
            $query->with('province');
        }])->first();
        return response(['status' => 200, 'user' => $user]);
    }

    public function count(Request $request)
    {
        $adminSettings = AdminSetting::find(1);
        $count = User::count();
        return response(['status' => 200, 'count' => $count, 'show' => $adminSettings->is_show_user_count]);
    }
}
