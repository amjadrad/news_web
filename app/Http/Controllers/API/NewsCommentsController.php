<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\NewsComment;
use Illuminate\Http\Request;

class NewsCommentsController extends Controller
{

    public function index(Request $request)
    {
        $comments = NewsComment::where('news_id', $request->news_id)
            ->whereNull('reply_id')
            ->where('active', 1)
            ->with(['comments' => function ($query) {
                $query->where('active', 1)->with(['user' => function ($query) {
                    $query->select('id', 'full_name')
                        ->where('active', 1);
                }])->orderBy('created_at', 'DESC');
            }, 'user' => function ($query) {
                $query->select('id', 'full_name');
            }])->orderBy('created_at', 'DESC')->paginate(20);

        return response(['status' => 200, 'data' => $comments]);
    }

    public function store(Request $request)
    {
        $comment = new NewsComment();
        $comment->news_id = $request->news_id;
        $comment->user_id = $request->user_id;
        $comment->reply_id = $request->reply_id;
        $comment->comment = $request->comment;
        $comment->active = 0;
        $comment->save();
        return response(['status' => 200], 200);
    }

    public function reply(Request $request)
    {
        $comment = new NewsComment();
        $comment->news_id = $request->news_id;
        $comment->user_id = $request->user_id;
        $comment->reply_id = $request->reply_id;
        $comment->comment = $request->comment;
        $comment->active = 0;
        $comment->save();
        return response(['status' => 200], 200);
    }

    public function adminIndex(Request $request)
    {
        $comments = NewsComment::where('news_id', $request->news_id)
            ->orderBy('created_at', 'DESC')
            ->with(['user' => function ($query) {
                $query->select('id', 'full_name');
            }])
            ->paginate(20);
        return response(['status' => 200, 'data' => $comments]);
    }

    public function adminActive(Request $request)
    {
        $newsComment = NewsComment::find($request->news_comment_id);
        $newsComment->active = $request->active;
        $newsComment->save();
        return response(['status' => 200, 'news_comment_id' => $request->news_comment_id, 'active' => $newsComment->active]);
    }

    public function adminSeen(Request $request)
    {
        $newsComment = NewsComment::find($request->news_comment_id);
        $newsComment->seen = 1;
        $newsComment->save();
        return response(['status' => 200, 'news_comment_id' => $request->news_comment_id]);
    }


}
