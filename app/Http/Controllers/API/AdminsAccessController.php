<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AdminAccess;
use Illuminate\Http\Request;

class AdminsAccessController extends Controller
{
    public function edit(Request $request)
    {
        $data = AdminAccess::where('admin_id', $request->admin_id)->get();
        return response(['status' => 200, 'admin_access' => $data]);
    }

    public function update(Request $request)
    {
        //news
        $adminAccessNews = AdminAccess::where('admin_id', $request->admin_id)->where('type', 'news')->first();
        if (!$adminAccessNews) {
            $adminAccessNews = new AdminAccess();
            $adminAccessNews->admin_id = $request->admin_id;
            $adminAccessNews->type = 'news';
        }
        $adminAccessNews->actions = $request->news_actions;
        $adminAccessNews->save();
        //categories
        $adminAccessCategories = AdminAccess::where('admin_id', $request->admin_id)->where('type', 'categories')->first();
        if (!$adminAccessCategories) {
            $adminAccessCategories = new AdminAccess();
            $adminAccessCategories->admin_id = $request->admin_id;
            $adminAccessCategories->type = 'categories';
        }
        $adminAccessCategories->actions = $request->categories_actions;
        $adminAccessCategories->save();
//        //user_news
        $adminAccessUserNews = AdminAccess::where('admin_id', $request->admin_id)->where('type', 'user_news')->first();
        if (!$adminAccessUserNews) {
            $adminAccessUserNews = new AdminAccess();
            $adminAccessUserNews->admin_id = $request->admin_id;
            $adminAccessUserNews->type = 'user_news';
        }
        $adminAccessUserNews->actions = $request->user_news_actions;
        $adminAccessUserNews->save();
//        //app_comments
        $adminAccessAppComments = AdminAccess::where('admin_id', $request->admin_id)->where('type', 'app_comments')->first();
        if (!$adminAccessAppComments) {
            $adminAccessAppComments = new AdminAccess();
            $adminAccessAppComments->admin_id = $request->admin_id;
            $adminAccessAppComments->type = 'app_comments';
        }
        $adminAccessAppComments->actions = $request->app_comments_actions;
        $adminAccessAppComments->save();
//        //user_contacts
        $adminAccessUserContacts = AdminAccess::where('admin_id', $request->admin_id)->where('type', 'user_contacts')->first();
        if (!$adminAccessUserContacts) {
            $adminAccessUserContacts = new AdminAccess();
            $adminAccessUserContacts->admin_id = $request->admin_id;
            $adminAccessUserContacts->type = 'user_contacts';
        }
        $adminAccessUserContacts->actions = $request->user_contacts_actions;
        $adminAccessUserContacts->save();

        return response(['status' => 200]);
    }

}
