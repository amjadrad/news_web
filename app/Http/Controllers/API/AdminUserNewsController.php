<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsMedia;
use App\Models\UserNews;
use App\Models\UserNewsMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminUserNewsController extends Controller
{

    public function index(Request $request)
    {
        $paginatedData = UserNews::orderBy('created_at', 'DESC')
            ->select('id', 'title', 'category_id', 'created_at')
            ->where('status', 'waiting')
            ->with('category');
        $paginatedData = $paginatedData->paginate(15);

        return response(['status' => 200, 'data' => $paginatedData, 'next_page_url' => $paginatedData->nextPageUrl()]);
    }

    public function show(Request $request)
    {
        $news = UserNews::where('id', $request->user_news_id)->with('category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function delete(Request $request)
    {
        $news = UserNews::with('medias')->find($request->user_news_id);
        foreach ($news->medias as $media) {
            Storage::disk('public')->delete($media->path);
        }
        $news->medias()->delete();
        $news->delete();
        return response(['status' => 200, 'user_news_id' => $request->user_news_id], 200);
    }

    public function store(Request $request)
    {
        $news = new UserNews();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->user_id = $request->user_id;
        $news->category_id = $request->category_id;
        $news->save();

        if ($request->media) {
            foreach ($request->media as $media) {
                $newMedia = new UserNewsMedia();
                $newMedia->user_news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }

        return response(['status' => 200, 'user_news_id' => $news->id], 200);
    }

    public function edit(Request $request)
    {
        $news = UserNews::where('id', $request->user_news_id)->with('medias', 'category')->first();
        return response(['status' => 200, 'news' => $news]);
    }

    public function update(Request $request)
    {
        $userNews = UserNews::find($request->user_news_id);
        $userNews->status = 'accepted';
        $userNews->save();
        $news = new News();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->link = $request->link;
        $news->admin_id = $request->admin_id;
        $news->category_id = $request->category_id;
        $news->active = $request->active;
        $news->type = $request->hot == 0 ? 'normal' : 'hot';
        $news->save();
        if ($request->media) {
            foreach ($request->media as $media) {
                $newMedia = new NewsMedia();
                $newMedia->news_id = $news->id;
                $newMedia->path = $media['path'];
                $newMedia->type = $media['type'];
                $newMedia->save();
            }
        }
        foreach ($userNews->medias as $media) {
            $newMedia = new NewsMedia();
            $newMedia->news_id = $news->id;
            $newMedia->path = $media['path'];
            $newMedia->type = $media['type'];
            $newMedia->save();
        }


        return response(['status' => 200, 'user_news_id' => $userNews->id], 200);
    }

    public function mediaDelete(Request $request)
    {
        $newsMedia = UserNewsMedia::find($request->media_id);
        Storage::disk('public')->delete($newsMedia->path);
        $newsMedia->delete();
        return response(['status' => 200, 'media_id' => $request->media_id], 200);
    }

    public function reject(Request $request)
    {
        $userNews = UserNews::find($request->user_news_id);
        $userNews->status = 'rejected';
        $userNews->save();
        return response(['status' => 200, 'user_news_id' => $userNews->id], 200);
    }


}
