<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AdminAccess;
use App\Models\AdminSetting;
use Illuminate\Http\Request;

class AdminSettingsController extends Controller
{
    public function edit(Request $request)
    {
        $data = AdminSetting::first();
        return response(['status' => 200, 'admin_settings' => $data]);
    }

    public function update(Request $request)
    {
        $adminSettings = AdminSetting::find(1);
        $adminSettings->is_show_user_count = $request->is_show_user_count;
        $adminSettings->save();
        return response(['status' => 200, 'admin_settings' => $adminSettings]);
    }

}
