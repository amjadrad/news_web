<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;

class CitiesController extends Controller
{

    public function provinces(Request $request)
    {
        $data = Province::all();
        return response(['status' => 200, 'provinces' => $data]);
    }

    public function cities(Request $request)
    {
        $data = City::where('province_id', $request->province_id)->get();
        return response(['status' => 200, 'cities' => $data]);
    }


}
