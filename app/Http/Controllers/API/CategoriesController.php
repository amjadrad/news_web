<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\Models\NewsVisit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class CategoriesController extends Controller
{

    public function index(Request $request)
    {
        $data = Category::withCount('news as news_count_unseen')->get();
        foreach ($data as $category) {
            $catNewsIds = $category->news()->select('id')->get();
            $countNewsUserSeen = NewsVisit::where('user_id', $request->user_id)
                ->whereIn('news_id', $catNewsIds)->get();
            $category->news_count_unseen = intval($category->news_count_unseen) - intval(count($countNewsUserSeen));
        }

        return response(['status' => 200, 'categories' => $data], 200);
    }

    public function adminShow(Request $request)
    {
        $category = Category::find($request->category_id);
        if ($category) {
            return response(['status' => 200, 'category' => $category], 200);
        } else {
            return response(['status' => 404], 404);
        }
    }

    public function adminStore(Request $request)
    {
        $category = new Category();
        $category->title = $request->title;
        $category->picture = $request->picture;
        $category->save();
        return response(['status' => 200, 'category' => $category]);
    }

    public function adminUpdate(Request $request)
    {
        $category = Category::find($request->category_id);
        if ($category) {
            $category->title = $request->title;
            if ($request->picture)
                $category->picture = $request->picture;
            $category->save();
            return response(['status' => 200, 'category' => $category]);
        } else {
            return response(['status' => 404], 404);
        }
    }

    public function adminDelete(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->delete();
        return response(['status' => 200, 'category_id' => $request->category_id]);
    }

}
