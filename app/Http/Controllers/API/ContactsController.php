<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function adminIndex(Request $request)
    {
        $data = Contact::orderBy('seen', 'ASC')
            ->with(['user' => function ($query) {
                $query->select('id', 'full_name');
            }])
            ->paginate(15);
        return response(['status' => 200, 'data' => $data, 'next_page_url' => $data->nextPageUrl()], 200);

    }

    public function adminDelete(Request $request)
    {
        $contact = Contact::find($request->contact_id);
        $contact->delete();
        return response(['status' => 200, 'contact_id' => $request->contact_id], 200);
    }

    public function adminSeen(Request $request)
    {
        $contact = Contact::find($request->contact_id);
        $contact->seen = 1;
        $contact->save();
        return response(['status' => 200, 'contact_id' => $request->contact_id], 200);
    }

    public function store(Request $request)
    {
        $contact = new Contact();
        $contact->user_id = $request->user_id;
        $contact->title = $request->title;
        $contact->description = $request->description;
        $contact->save();
        return response(['status' => 200, 'contact_id' => $contact->id], 200);
    }
}
