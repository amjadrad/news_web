<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public function comments()
    {
        return $this->hasMany(NewsComment::class);
    }

    public function likes()
    {
        return $this->hasMany(NewsLike::class)->where('like', 1);
    }

    public function dislikes()
    {
        return $this->hasMany(NewsLike::class)->where('like', 0);
    }

    public function favorite()
    {
        return $this->hasOne(UserNewsFavorite::class);
    }

    public function medias()
    {
        return $this->hasMany(NewsMedia::class);
    }

    public function visits()
    {
        return $this->hasMany(NewsVisit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function getUserLike($userId)
    {
        return NewsLike::select('like')->where('news_id', $this->id)->where('user_id', $userId)->first()->like ?? null;
    }

    public function getUserVisit($userId)
    {
        return NewsVisit::select('news_id')->where('news_id', $this->id)->where('user_id', $userId)->first() ?true:false;
    }

    public function getCreatedAtAttribute($value)
    {
        return Verta($value)->formatDatetime();
    }

}
