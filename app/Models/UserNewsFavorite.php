<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNewsFavorite extends Model
{
    protected $table = 'user_news_favorites';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function news()
    {
        return $this->belongsTo(News::class);
    }


}
