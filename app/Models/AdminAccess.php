<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminAccess extends Model
{
    protected $table = 'admin_access';
    public $timestamps = false;

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
