<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNews extends Model
{
    protected $table = 'user_news';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function medias()
    {
        return $this->hasMany(UserNewsMedia::class);
    }
}
