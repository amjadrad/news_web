<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminSetting extends Model
{
    protected $table = 'admin_settings';
    public $timestamps = false;

    protected $casts = [
        'is_show_user_count' => 'boolean',
    ];

}
