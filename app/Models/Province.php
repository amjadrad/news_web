<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function centerCity()
    {
        return $this->belongsTo(City::class, 'center_city_id', 'id');
    }

}
