<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsVisit extends Model
{
    protected $table = 'news_visits';
    public $timestamps = false;

    public function news()
    {
        return $this->belongsTo(News::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
