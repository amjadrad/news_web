<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppComment extends Model
{
    protected $table = 'app_comments';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
