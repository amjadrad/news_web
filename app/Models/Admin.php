<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admins';

    public function news()
    {
        return $this->hasMany(News::class);
    }
}
