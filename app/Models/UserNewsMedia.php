<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNewsMedia extends Model
{
    protected $table = 'user_news_media';
    public $timestamps = false;

    public function userNews()
    {
        return $this->belongsTo(UserNews::class);
    }
}
