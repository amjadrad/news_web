<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsMedia extends Model
{
    protected $table = 'news_media';
    public $timestamps = false;

    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
