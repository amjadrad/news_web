<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsComment extends Model
{
    protected $table = 'news_comments';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function news()
    {
        return $this->belongsTo(News::class);
    }

    public function comments()
    {
        return $this->hasMany(NewsComment::class, 'reply_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Verta($value)->formatDifference();
    }
}
