<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsPoster extends Model
{
    protected $table = 'news_posters';
    public $timestamps = false;

    public function news()
    {
        return $this->belongsTo(News::class);
    }

}
